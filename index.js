const http = require('http');
const fs = require('fs/promises');

const port = process.env.PORT || 5000;


const comp_list = new Promise (function(resolve, reject) {
	fs.readFile('./company_html_search3933432135fc5356dca544.html', (err, data) => {
		err ? reject(err) : resolve(data);
	});
});
http.createServer((req, res) => {
	res.setHeader('Content-Type': 'text/html');
	res.write(200, comp_list.then((data) => {return data;}));
}).listen(port);
